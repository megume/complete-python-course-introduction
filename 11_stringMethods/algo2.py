'''Prints the running times for problem sizes that double, using a single loop
Analyzing an algorithm that counts instruction in the high-level code:
1. Instructions that execute the same number of times regardless of problem size
2. Instructions whose execution count varies with the problem size.

'''

import time

problemSize = 1000
print("%12s%15s" % ("Problem size","Iterations"))
for count in range(5):
    start = time.time()
    number = 0
    # Start of the algorithm
    work = 1
    for x in range(problemSize):
        for y in range(problemSize):
            number += 1
            work +=1
            work -=1

    # end of the algorithm
    elapsed = time.time() - start
    print("%12d%15d" % (problemSize, number))
    problemSize *=2